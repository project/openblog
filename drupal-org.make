api = 2

core = 7.x

; Modules
projects[ctools][version] = "1.9"
projects[ctools][subdir] = "contrib"

projects[codefilter][version] = "1.2"
projects[codefilter][subdir] = "contrib"

projects[context][version] = "3.6"
projects[context][subdir] = "contrib"

projects[entity][version] = "1.7"
projects[entity][subdir] = "contrib"

projects[entitycache][version] = "1.5"
projects[entitycache][subdir] = "contrib"

projects[diff][version] = "3.2"
projects[diff][subdir] = "contrib"

projects[features][version] = "2.10"
projects[features][subdir] = "contrib"

projects[filefield_sources][version] = "1.10"
projects[filefield_sources][subdir] = "contrib"

projects[fontyourface][version] = "2.8"
projects[fontyourface][subdir] = "contrib"

projects[gist_filter][version] = "1.2"
projects[gist_filter][subdir] = "contrib"

projects[globalredirect][version] = "1.5"
projects[globalredirect][subdir] = "contrib"

projects[google_analytics][version] = "2.1"
projects[google_analytics][subdir] = "contrib"

projects[honeypot][version] = "1.22"
projects[honeypot][subdir] = "contrib"

projects[insert][version] = "1.3"
projects[insert][subdir] = "contrib"

projects[libraries][version] = "1.0"
projects[libraries][subdir] = "contrib"

projects[markdown][version] = "1.2"
projects[markdown][subdir] = "contrib"

projects[metatag][version] = "1.14"
projects[metatag][subdir] = "contrib"

projects[pathauto][version] = "1.3"
projects[pathauto][subdir] = "contrib"

projects[profile2][version] = "1.3"
projects[profile2][subdir] = "contrib"

projects[r4032login][version] = "1.8"
projects[r4032login][subdir] = "contrib"

projects[redirect][version] = "1.0-rc3"
projects[redirect][subdir] = "contrib"

projects[role_delegation][version] = "1.1"
projects[role_delegation][subdir] = "contrib"

projects[sharethis][version] = "2.12"
projects[sharethis][subdir] = "contrib"

projects[textformatter][version] = "1.4"
projects[textformatter][subdir] = "contrib"

projects[token][version] = "1.6"
projects[token][subdir] = "contrib"

projects[video_filter][version] = "3.3"
projects[video_filter][subdir] = "contrib"

projects[views][version] = "3.13"
projects[views][subdir] = "contrib"

; Themes
projects[omega][version] = "3.1"
projects[omega][subdir] = "contrib"

projects[subtle_simplicity][version] = "2.0-alpha1"
projects[subtle_simplicity][subdir] = "contrib"
